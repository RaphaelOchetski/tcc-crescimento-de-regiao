import cv2
from skimage import feature, filters
from matplotlib.pyplot import show
from PIL import Image

img = cv2.imread('result.jpg', 0)

edges_canny = feature.canny(img) # Canny
# edges_sobel = filters.sobel(img) # Sobel
# edges_laplace = filters.laplace(img) # Laplacian
# edges_scharr = filters.scharr(img) # Scharr
# # edges_prewitt = filters.prewitt(img) # Prewitt
# # edges_roberts = filters.roberts(img) # Roberts
my_img = Image.fromarray(edges_canny)
my_img.show()

