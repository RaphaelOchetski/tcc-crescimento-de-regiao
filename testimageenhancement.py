from PIL import Image, ImageEnhance
import glob

image_list = []

for filename in glob.glob('fundus_images_bkp_filtered/*jpg'):
    im = Image.open(filename)
    image_list.append(im)

for n in image_list:
    img = Image.open(n.filename)
    en = ImageEnhance.Contrast(img)
    img = en.enhance(3)
    img.save(n.filename)

# img = Image.open('newfiltered.png')
# img.show()
# en = ImageEnhance.Contrast(img)
# img = en.enhance(4)
# img.show()
# img.save('contrast_filtered.png')



