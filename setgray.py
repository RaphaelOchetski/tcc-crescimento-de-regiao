from PIL import Image, ImageFilter, ImageEnhance
import cv2
import glob
import numpy

image_list = []

for filename in glob.glob('fundus_images_bkp_filtered_gray/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for n in image_list:
    img = cv2.cvtColor(cv2.imread(n.filename), cv2.COLOR_BGR2GRAY)
    cv2.imwrite(n.filename, img)