from PIL import Image
import glob

image_list = []

for filename in glob.glob('new_gt/*.png'):
    im = Image.open(filename)
    image_list.append(im)

basewidth = 1368

for img in image_list:

    resized = Image.open(img.filename)
    wpercent = (basewidth/float(resized.size[0]))
    hsize = int((float(resized.size[1])*float(wpercent)))
    resized = resized.resize((basewidth,hsize), Image.ANTIALIAS)
    resized.save(img.filename)