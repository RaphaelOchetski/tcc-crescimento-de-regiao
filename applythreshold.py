import cv2
from PIL import Image
import glob

image_list = []

for filename in glob.glob('threshold_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for img in image_list:
    actual = cv2.imread(img.filename)
    retval, threshold = cv2.threshold(actual, 210, 255, cv2.THRESH_BINARY)
    cv2.imwrite(img.filename, threshold)