import cv2
from PIL import Image

image = cv2.imread('threshold_exits/IDRiD_06.jpg')
retval, threshold = cv2.threshold(image, 210, 255, cv2.THRESH_BINARY)
cv2.imwrite('threshold_exits/IDRiD_06.jpg', threshold)