import cv2
from PIL import Image
import glob

image_list = []

for filename in glob.glob('kmeans_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for img in image_list:
    image = cv2.imread(img.filename)

    binary = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    height = image.shape[0]
    width = image.shape[1]


    for y in range(height):
        for x in range(width):
            if(0 <= y + 70 <= 907 and 0 <= x + 70 <= 1367):
                if binary[y-60, x] == 255 and binary[y+60, x] == 255 and binary[y, x+33] == 255 and binary[y, x-33] == 255:
                    cv2.circle(image, (x, y), 80, (0, 0, 0), -1)
#cv2.circle(image, (180, 310), 95, (255,0,0), -1)
#cv2.imshow('test', image)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
    cv2.imwrite(img.filename, image)