import cv2
import matplotlib.pyplot as plt

fundus = cv2.imread('filtered/IDRiD_10.jpg', 0)
ret, test = cv2.threshold(fundus, 40, 100, cv2.THRESH_BINARY)
fundus_non_filtered = cv2.imread('resized/IDRiD_10.jpg', 0)
ret, non_filtered = cv2.threshold(fundus_non_filtered, 40, 80, cv2.THRESH_BINARY)
cv2.imshow('gray filtered', test)
cv2.imshow('non filtered', non_filtered)
cv2.waitKey()