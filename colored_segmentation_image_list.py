import cv2
from PIL import Image
import glob

image_list = []

for filename in glob.glob('kmeans_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for img in image_list:
    actual = cv2.imread(img.filename)
    img_hsv = cv2.cvtColor(actual, cv2.COLOR_BGR2HSV)

    ## Gen lower mask (0-5) and upper mask (175-180) of RED
    mask = cv2.inRange(img_hsv, (20,0,0), (31,255,255))

    ## Merge the mask and crop the red regions
    croped = cv2.bitwise_and(actual, actual, mask=mask)

    gray = cv2.cvtColor(croped, cv2.COLOR_BGR2GRAY)
    ret, threshold = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(img.filename, threshold)