import numpy as np
import imageio
import glob
import statistics
from PIL import Image

image_list = []
gt_list = []
results = []

for filename in glob.glob('kmeans_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for filename in glob.glob('hard_exudates_ground_truth/*.png'):
    im = Image.open(filename)
    gt_list.append(im)

# img = imageio.imread('threshold_exits/IDRiD_02.jpg')
# gt = imageio.imread('hard_exudates_ground_truth/IDRiD_02_EX.png')

for n in range(len(gt_list)):
    img = imageio.imread(image_list[n].filename)
    gt = imageio.imread(gt_list[n].filename)
    intersection = np.logical_and(gt, img)
    union = np.logical_or(gt, img)
    iou_score = np.sum(intersection) / np.sum(union)
    results.append(iou_score)
    print(str(n+1) +': ' + str(iou_score))

print('Media: ' + str(statistics.mean(results)))
print('Desvio padrao: ' + str(statistics.pstdev(results, mu=None)))