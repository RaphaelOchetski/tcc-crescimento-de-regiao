import cv2
import numpy as np
from matplotlib.colors import hsv_to_rgb
import matplotlib.pyplot as plt

## Read and merge
img = cv2.imread("kmeans.jpg")
img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

## Gen lower mask (0-5) and upper mask (175-180) of RED
mask = cv2.inRange(img_hsv, (20,0,0), (31,255,255))

light_orange =  (31, 255, 255)
dark_orange = (20,0,0)

lo_square = np.full((10, 10, 3), light_orange, dtype=np.uint8) / 255.0
do_square = np.full((10, 10, 3), dark_orange, dtype=np.uint8) / 255.0

plt.subplot(1, 2, 1)
plt.imshow(hsv_to_rgb(do_square))
plt.subplot(1, 2, 2)
plt.imshow(hsv_to_rgb(lo_square))
plt.show()

## Merge the mask and crop the red regions
croped = cv2.bitwise_and(img, img, mask=mask)

gray = cv2.cvtColor(croped, cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)

## Display
cv2.imshow("mask", mask)
cv2.imshow("croped", croped)
cv2.imshow('threshold', threshold)
cv2.imwrite('croped.png', threshold)
cv2.waitKey()