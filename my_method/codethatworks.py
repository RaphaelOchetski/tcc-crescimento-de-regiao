import cv2
import numpy
import numpy as np
from PIL import Image
from timeit import default_timer as timer
import datetime
import glob

def get8n(x, y, f_height, f_width):
    out = []
    maxx = f_width - 1
    maxy = f_height - 1

    # top left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top center
    outx = x
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # left
    outx = min(max(x - 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # right
    outx = min(max(x + 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # bottom left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom center
    outx = x
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    return out


def region_growing(image, seed):
    width = image.shape[1]
    height = image.shape[0]
    b, g, r = image[seed[1], seed[0]]
    outimg = np.zeros_like(image)

    for h in range(height):
        for w in range(width):
            list = []
            list.append((h, w))
            processed = []
            print('height: ' + str(h) + 'width: ' + str(w))
            while (len(list) > 0):
                for n in processed:
                    image[n[0],n[1]] = [0,0,0]
                pix = list[0]
                if r - 5 <= image[h, w][2] <= r + 5 and g - 5 <= image[h, w][1] <= g + 5 and b - 5 <= image[h, w][0] <= b + 5:
                    outimg[pix[0], pix[1]] = [255,255,255]
                    for coord in get8n(pix[0], pix[1], height, width):
                        if r - 20 <= image[coord[0], coord[1]][2] <= r + 20 and g - 10 <= image[coord[0], coord[1]][1] <= g + 10 and b - 5 <= image[coord[0], coord[1]][0] <= b + 5:
                            outimg[coord[0], coord[1]] = [255,255,255]
                            if not coord in processed:
                                list.append(coord)
                            processed.append(coord)
                list.pop(0)
                # cv2.imwrite('exits/out' + str(n) + '.png', outimg)
                cv2.imshow("progress", outimg)
                cv2.waitKey(1)
            # return outimg
    cv2.imwrite('exits/1/1.png', outimg)
    return outimg


def on_mouse(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('Seed: ' + str(x) + ', ' + str(y), colored_image[y, x])
        clicks.append((x, y))


# for filename in glob.glob('fundus_images_resized/*.jpg'):
#     im = cv2.imread(filename, 1)
#     image_list.append(im)

clicks = []
colored_image = cv2.imread('fundus_images_resized/IDRiD_01.jpg', 1)
cv2.namedWindow('Input')
cv2.setMouseCallback('Input', on_mouse, 0, )
cv2.imshow('Input', colored_image)
cv2.waitKey()
seed = clicks[-1]
print('Started at: ', datetime.datetime.now())
start = timer()
out = region_growing(colored_image, seed)
cv2.imshow('Region Growing', out)
print('Duration: ', str(timer() - start) + 's')
cv2.waitKey()
cv2.destroyAllWindows()
