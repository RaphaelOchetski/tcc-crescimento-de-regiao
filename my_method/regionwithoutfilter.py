import cv2
import numpy as np
from PIL import Image
from timeit import default_timer as timer
import datetime

def get8n(x, y, shape):
    out = []
    maxx = shape[1] - 1
    maxy = shape[0] - 1

    # top left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top center
    outx = x
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # left
    outx = min(max(x - 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # right
    outx = min(max(x + 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # bottom left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom center
    outx = x
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    return out


def region_growing(gray_image):
    width = gray_image.shape[1]
    height = gray_image.shape[0]
    outimg = np.zeros_like(gray_image)
    for h in range(height):
        for w in range(width):
            list = []
            list.append((h, w))
            processed = []
            #print('height: ' + str(h))
            #print('width: ' + str(w))
            while (len(list) > 0):
                pix = list[0]
                #print(gray_image[h, w])
                if intensity - 15 <= gray_image[h, w] <= intensity + 15:
                    outimg[pix[0], pix[1]] = 255
                for coord in get8n(pix[0], pix[1], Image.fromarray(gray_image).size):
                    if intensity - 15 <= gray_image[coord[0], coord[1]] <= intensity + 15:
                        outimg[coord[0], coord[1]] = 255
                        gray_image[coord[0], coord[1]] = 0
                        if not coord in processed:
                            list.append(coord)
                        processed.append(coord)
                list.pop(0)
                cv2.imshow("progress", outimg)
                cv2.waitKey(1)
    cv2.imwrite('sem_filtro_exits/out3.png', outimg)
    return outimg


def on_mouse(event, x, y, flags, params):
    if event == cv2.EVENT_LBUTTONDOWN:
            print('Seed: ' + str(x) + ', ' + str(y), filtered_image[y, x])
            clicks.append((y, x))

clicks = []
image = cv2.imread('fundus_images_resized/IDRiD_3.jpg')
# image = cv2.imread('contrast_filtered.png')

filtered_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

#cv2.namedWindow('Input')
#cv2.setMouseCallback('Input', on_mouse, 0, )
cv2.imshow('Input', filtered_image)
cv2.waitKey()
#seed = clicks[-1]
start = timer()
intensity = filtered_image[465, 588]
print(intensity)
print('Started at: ', datetime.datetime.now())
out = region_growing(filtered_image)
cv2.imshow('Region Growing', out)
print('Duration: ', str(timer() - start) + 's')
cv2.waitKey()
cv2.destroyAllWindows()
