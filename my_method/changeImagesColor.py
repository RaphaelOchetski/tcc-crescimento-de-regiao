from PIL import Image
import glob

image_list = []

for filename in glob.glob('optic_discs/*.png'):
    im = Image.open(filename)
    image_list.append(im)

for img in image_list:

    image = Image.open(img.filename)
    width, height = image.size
    rgb_img = image.convert('RGB')

    redcolor = 255
    blackcolor = (0,0,0)
    whitecolor = (255,255,255)

    for x in range(width):
        for y in range(height):
            r, g, b = rgb_img.getpixel((x, y))
            if r == redcolor:
                rgb_img.putpixel((x, y), whitecolor)
            else:
                rgb_img.putpixel((x, y), whitecolor)

    rgb_img.save(image.filename)
