from PIL import Image
import glob

image_list = []

# img = Image.open("exits/out0.png")
#
# background = Image.open("exits/out1.png")
#
# background.paste(img, (0, 0), img)
# background.save('how_to_superimpose_two_images_01.png', "PNG")

for filename in glob.glob('exits/*.png'):
    im = Image.open(filename)
    image_list.append(im)

img = image_list[0]
#
for n in range(len(image_list)):
    img.paste(image_list[n], (0, 0), image_list[n])

img.save('final_img.png', "PNG")