import cv2
import numpy as np

image_filtered = cv2.imread('filtered.jpg', 0)
image_resized = cv2.imread('resized.jpg', 0)

ret, img_filtered = cv2.threshold(image_filtered, 150, 255, cv2.THRESH_BINARY)
ret2, img_resized = cv2.threshold(image_resized, 150, 255, cv2.THRESH_BINARY)

cv2.imshow('Filtered', img_filtered)
cv2.imshow('Resized', img_resized)
cv2.waitKey()

# image_resized = cv2.imread('resized.jpg', 0)
# img_resized = cv2.threshold(image_resized, 135, 255, cv2.THRESH_BINARY)
# cv2.imshow('Resized', img_resized)