import cv2
import numpy as np
from PIL import Image

#exit = Image.open('new_exits/out1.png').convert('L')
exit = cv2.imread('sem_filtro_exits/out48.png')
#gt = Image.open('hard_exudates_ground_truth/IDRiD_01_EX.png').convert('L')
gt = cv2.imread('hard_exudates_ground_truth/IDRiD_48_EX.png')


# exit = cv2.imread('exits/outimg.png', 0)
width = exit.shape[1]
height = exit.shape[0]
total_pixels = width*height

# file = open('accuracy.txt', 'w')

# for i in range(len(exit_list)):

correct = 0
incorrect = 0
success = 0
error = 0
gt_counter = 0
exit_pix_counter = 0
total_blank = 0

# gt = cv2.imread('hard_exudates_ground_truth/IDRiD_10_EX.png', 0)

for y in range(height):
    for x in range(width):

        if gt[y, x][0] == 255 and gt[y, x][1] == 255 and gt[y, x][2] == 255:
            gt_counter += 1

        if  exit[y, x][0] == 255 and exit[y, x][1] == 255 and exit[y, x][2] == 255:
            total_blank += 1

        if  exit[y, x][0] == 255 and exit[y, x][1] == 255 and exit[y, x][2] == 255 and  gt[y, x][0] == 255 and gt[y, x][1] == 255 and gt[y, x][2] == 255:
            correct += 1

        if  exit[y, x][0] == 255 and exit[y, x][1] == 255 and exit[y, x][2] == 255 and  gt[y, x][0] == 0 and gt[y, x][1] == 0 and gt[y, x][2] == 0:
            incorrect += 1


gr_percentage = (gt_counter/total_pixels) * 100
success = (correct/gt_counter) * 100
error = (incorrect/(total_pixels)) * 100
total = (total_blank/total_pixels) * 100

print('Branco na imagem de saida: ' + str(total) + '% da imagem')
print('Branco na imagem de ground truth: ', str(gr_percentage) + '% da imagem')
print('Taxa de acerto comparado ao ground truth: ', str(success) + '%')
print('Marcado erroneamente: ', str(error) + '% da imagem')

# file.write('Imagem: ' + str(i))
# file.write('Taxa de acerto: ' + str(success) + '%')
# file.write('Marcado erroneamente: ' + str(error) + '%\n\n')
#
# file.close()