from PIL import Image

fundus = Image.open('IDRiD_54.jpg')
optic_disc = Image.open('gt54.png')

width, height = fundus.size
rgb_img = fundus.convert('RGB')

blackcolor = (0,0,0)

for x in range(width):
    for y in range(height):
        rf, rg, rb = optic_disc.getpixel((x,y))
        if (rf,rg,rb) == blackcolor:
            fundus.putpixel((x, y), blackcolor)

fundus.save(fundus.filename)