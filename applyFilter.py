from PIL import Image, ImageFilter, ImageEnhance
import cv2
import glob
import numpy

image_list = []

for filename in glob.glob('fundus_images_bkp_filtered/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)


for n in image_list:
    im_pil = n.filter(ImageFilter.EDGE_ENHANCE)
    im_opencv = cv2.cvtColor(numpy.array(im_pil), cv2.COLOR_RGB2BGR)
    cv2.imwrite(n.filename, im_opencv)