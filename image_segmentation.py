import cv2
import matplotlib.pyplot as plt
import numpy as np

fundus = cv2.imread('fundus_images_bkp_filtered/IDRiD_07.jpg')
hsv = cv2.cvtColor(fundus, cv2.COLOR_BGR2HSV)

h, s, v = cv2.split(hsv)

width = fundus.shape[1]
height = fundus.shape[0]

hlight = h[614, 548]
slight = s[614, 548]
vlight = v[614, 548]

hdark = h[705, 347]
sdark = s[705, 347]
vdark = v[705, 347]

light_orange = (26, 59, 255)
dark_orange = (13, 232, 255)

mask = cv2.inRange(hsv, dark_orange, light_orange)

result = cv2.bitwise_and(fundus, fundus, mask=mask)

cv2.imshow('result', result)
cv2.waitKey()