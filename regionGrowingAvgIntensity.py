import cv2
import numpy as np
from PIL import Image
import glob

def get8n(x, y, shape):
    out = []
    maxx = shape[1] - 1
    maxy = shape[0] - 1

    # top left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top center
    outx = x
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # top right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y - 1, 0), maxy)
    out.append((outx, outy))

    # left
    outx = min(max(x - 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # right
    outx = min(max(x + 1, 0), maxx)
    outy = y
    out.append((outx, outy))

    # bottom left
    outx = min(max(x - 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom center
    outx = x
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    # bottom right
    outx = min(max(x + 1, 0), maxx)
    outy = min(max(y + 1, 0), maxy)
    out.append((outx, outy))

    return out


def region_growing(gray_image):
    width = gray_image.shape[1]
    height = gray_image.shape[0]
    outimg = np.zeros_like(gray_image)
    for h in range(height):
        for w in range(width):
            list = []
            list.append((h, w))
            processed = []
            #print('height: ' + str(h))
            #print('width: ' + str(w))
            while (len(list) > 0):
                pix = list[0]
                #print(gray_image[h, w])
                if intensity - 25 <= gray_image[h, w] <= intensity + 25:
                    outimg[pix[0], pix[1]] = 255
                for coord in get8n(pix[0], pix[1], Image.fromarray(gray_image).size):
                    if intensity - 25 <= gray_image[coord[0], coord[1]] <= intensity + 25:
                        outimg[coord[0], coord[1]] = 255
                        gray_image[coord[0], coord[1]] = 0
                        if not coord in processed:
                            list.append(coord)
                        processed.append(coord)
                list.pop(0)
                # cv2.imshow("progress", outimg)
                # cv2.waitKey(1)
    return outimg


image_list = []
intensity = 210

for filename in glob.glob('region_growing_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for img in image_list:
    image = cv2.imread(img.filename, 0)
    cv2.waitKey()
    out = region_growing(image)
    cv2.imwrite(img.filename, out)




