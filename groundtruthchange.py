from PIL import Image
import glob

image_list = []

for filename in glob.glob('new_gt/*.png'):
    im = Image.open(filename)
    image_list.append(im)

width, height = image_list[0].size

whitecolor = (255, 255, 255)
blackcolor = (0, 0, 0)
redcolor = 255
for img in image_list:
    image = Image.open(img.filename)
    rgb_img = image.convert('RGB')
    for x in range(width):
        for y in range(height):
            r, g, b = rgb_img.getpixel((x, y))
            if r == redcolor:
                rgb_img.putpixel((x, y), whitecolor)
    rgb_img.save(image.filename)
