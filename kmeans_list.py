import glob
import numpy as np
import cv2
from PIL import Image
import matplotlib.pyplot as plt

image_list = []

for filename in glob.glob('kmeans_exits/*.jpg'):
    im = Image.open(filename)
    image_list.append(im)

for image in image_list:
    img = cv2.imread(image.filename)

    img=cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

    vectorized = img.reshape((-1, 3))

    vectorized = np.float32(vectorized)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

    K = 4
    attempts=10
    ret,label,center=cv2.kmeans(vectorized,K,None,criteria,attempts,cv2.KMEANS_PP_CENTERS)

    center = np.uint8(center)

    res = center[label.flatten()]
    result_image = res.reshape((img.shape))

    result_image = cv2.cvtColor(result_image, cv2.COLOR_BGR2RGB)
    cv2.imwrite(image.filename, result_image)